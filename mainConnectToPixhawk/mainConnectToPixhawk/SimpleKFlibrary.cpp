#include "SimpleKFlibrary.h"
#include "Arduino.h"

SimpleKf::SimpleKf()
{ 
	// code executes at instancing of class
	 _x_prev = 0;
	 _x_now = 0;
	 _Pk_now = 0;
	 _Pk_prev = 1;
	 _x_est = 0;
	 _R = 0;
	 _Q = 0;
	 _K = 1;
	_output_kalman = 0;
	_kf_out = 0;
	_u=0;
	_K_out=0;
	 _input=0;
	 }

int SimpleKf::defineRQ(float input1, float input2) {
	_Q = input2; //1/5;
	_R = input1; //1;
}

float SimpleKf::calculateKF(float input)
{
	// this is function inside class with float input and return of float _x_now;
	_input = input;
	//time update
	_x_now = _x_prev;
	_Pk_now = _Pk_prev + _Q;
	//	% mesurement update
	_K = _Pk_prev / (_Pk_prev + _R);// % Kalman gain.Tells how much should we trust our observations
	_x_est = _x_now + _K * (_input - _x_now);
	_Pk_now = (1 - _K)*_Pk_now;
	_Pk_prev = _Pk_now;
	_x_prev = _x_est;
	return _x_prev;
}
