#ifndef t1
#define t1

#if (ARDUINO >=100)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
class SimpleKf
{
public:
	// constructor
	SimpleKf();
	int  defineRQ(float input1, float input2);
	float calculateKF(float input);
	

private:
	
	float _x_prev ;
	float _x_now ;
	float _Pk_now ;
	float _Pk_prev ;
	float _x_est ;
	float _R;
	float _Q;
	float _K;
	float _output_kalman ;
	float _kf_out ;
	float _u;
	float _K_out;
	float _input;
};

#endif


