#include <TimerOne.h> //SBUS write timer
#include <SBUS.h>	 // SBUS library
#include <PulsePosition.h> //CPPM library 
#include "eeprom.h" // za citanje faktora kalibracije vage i kalibriranje
#include "math.h" // mozda zatreba matematika
#include <Servo.h> // za slanje pwma za upravljanje servomotorom
#include "SimpleKFlibrary.h"
#define HWSERIAL1 Serial1 // Serijski port za pisanje SBUSa na pixhawk
#define HWSERIAL3 Serial3 // serijski port za �itanje podataka sa pixhawka
#define BATT_VOLT_PIN  14 // voltage read pin
#define BATT_CURR_PIN  15 // current read pin

float mesured_voltage; //This will store the converted data
float mesured_current;
float BATT_VOLT_SCALING = 10.72012615; // - scaling coefficient for voltage sensor
float BATT_CURR_SCALING = 18.5; //scaling coefficient for current sensor

Servo myservo;  // create servo object to control a servo 
				// a maximum of eight servo objects can be created 


const int numOfDataBytes = 40;// ocekujemo ukupno 12 byteova
byte incomingBytes[numOfDataBytes];  // for incoming serial data

float readedFloatsInput[10];
float input1 = 0;
float input2 = 0;
float input3 = 0;
float input4 = 0;
float input5 = 0;
float input6 = 0;
float input7 = 0;
float input8 = 0;
float input9 = 0;
float input10 = 0;

byte data1[4];
byte data2[4];
byte data3[4];
byte data4[4];
byte data5[4];
byte data6[4];
byte data7[4];
byte data8[4];
byte data9[4];
byte data10[4];
byte datain[4];
float u1 = 0;
float u2 = 0;

int newdata;
int prev_data;
float arrayFloat[10];
bool hederDone = false;
int headercounter = 0;
int datacounter = -1;
int lengthcounter = -1;

SBUS SBUSToPX4(HWSERIAL1);// a SBUS object called SBUSToPX4, which is on Teensy hardware serial port 1 (pin 1)
PulsePositionOutput myOut;// output data to write,  pin 9 -- not working with PX4
PulsePositionInput myIn; // incomin data to read,  pin 10

/// Global VARIABLES//
uint16_t radioChannels[8]; // size for 8 input channels,radio channels values GLOBAL STATUS
uint16_t channels[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // write SBUS channels structure

SimpleKf object1;
SimpleKf object2;

														 // the setup function runs once when you press reset or power the board
void setup() {
	SBUSToPX4.begin();				  // begin the SBUS communication		   
	Timer1.initialize(9000); //setup an interrupt to send packets every 9 ms
	Timer1.attachInterrupt(sendSBUSandReadPPM);
	myOut.begin(9); // init CPPM PINS, write  (pin 9) and read (pin 10) 
	myIn.begin(11); //preba�eno s 10 na 11
					//analogWriteResolution(8); // 16 bits ersolution 
	analogWriteFrequency(6, 100); // output pin and frequency in Hz
	myservo.attach(6);

	pinMode(BATT_VOLT_PIN, INPUT);
	pinMode(BATT_CURR_PIN, INPUT);
	analogReadResolution(10);
	analogReadAveraging(5);
		
	Serial.begin(57600); // output to console for debugging (USB serial connection)
	HWSERIAL3.begin(115200);     // opens serial port 3, sets data rate to 9600 bps
	while (!HWSERIAL3) {};// wait for serial port to connect.

	object1.defineRQ(1, 1 / 5);
	object2.defineRQ(1, 1 );
	}



// the loop function runs over and over again until power down or reset
void loop() {
	//serialRawRead(); // read serial data
	//printSerialReadData();



	readPowerModuleCurrentVoltage();
	


	//sendPWM(a);
	//printReceivedPPM();
	//printGlobalPPM();
}



void readPowerModuleCurrentVoltage() {
	//Measurement
	int raw_read_voltage = analogRead(BATT_VOLT_PIN);
	int raw_read_current = analogRead(BATT_CURR_PIN);

	//Conversion of the analog reading(which goes from 0 - 1023) to a voltage(0 - 3,3V) :
	float read_voltage = raw_read_voltage * 3.3 / 1023.0;
	float read_current = raw_read_current * 3.3 / 1023.0;

	mesured_voltage = read_voltage * BATT_VOLT_SCALING;
	mesured_current = read_current * BATT_CURR_SCALING;
	Serial.print(mesured_voltage);
	float newmes ;
	float newmes2 ;
	newmes = object1.calculateKF(mesured_voltage);
	Serial.print("   Volts   ");
	Serial.print(newmes);
	newmes2 = object2.calculateKF(mesured_current);
	Serial.print("   Amps  kf ");
	Serial.print(newmes2);
	Serial.print("   Amps mes   ");
	Serial.println(mesured_current);
	//Display
	//Serial.print("   Volts   ");
	//Serial.print(newmes);
	//Serial.print("   Amps   ");
	//Serial.print("   ");
	//Serial.println(mesured_current);

	//delay(200);


}



// funkcija za generiranje PWM, cmd je od 1000 do 2000 
void sendPWM(float Cmd) {
	




}

int serial_writedata() {
	// to be done;


}


// funkcija �ita pakete byteva sa serial porta
int serialRawRead() {
	if (HWSERIAL3.available() > 0) {

		newdata = HWSERIAL3.read();
		if (prev_data == 13 && newdata == 13) {
			headercounter++;
			datacounter = -1;
			//lengthcounter = -1;
			}
		else
		{
			datacounter++;
			headercounter = 0;
			if (datacounter >= 0 && datacounter <40) {
				incomingBytes[datacounter] = newdata;
				//Serial.print(datacounter);
				//Serial.print(" ");
				//Serial.print(incomingBytes[3]);
				//Serial.print(" incomingBytes");
				//Serial.print(newdata);
				//Serial.print(" ");
			}
		}


		if (datacounter == 39) {
			serial_parse_raw_bytes();
			//Serial.print(newdata);
			//Serial.print(" ");
			//serial_print();
			//serial_print();		
		}
		
		/*Serial.print(newdata);
		Serial.print(" ");
		Serial.print(headercounter);
		Serial.print(" ");
		Serial.print(datacounter);
		Serial.println(" ");*/
		prev_data = newdata;
	}
} 

// funkcija parsira 4byte sa seriala u float varijable u global
int serial_parse_raw_bytes() {
	//serial_print();
	//Serial.println();
	datain[0] = incomingBytes[0];
	datain[1] = incomingBytes[1];
	datain[2] = incomingBytes[2];
	datain[3] = incomingBytes[3];
	input1 = readFloat(datain);//*((float*)(data1));
	arrayFloat[0] = input1;

	datain[0] = incomingBytes[4];
	datain[1] = incomingBytes[5];
	datain[2] = incomingBytes[6];
	datain[3] = incomingBytes[7];
	input2 = readFloat(datain);//*((float*)(data2));
	arrayFloat[1] = input2;

	datain[0] = incomingBytes[8];
	datain[1] = incomingBytes[9];
	datain[2] = incomingBytes[10];
	datain[3] = incomingBytes[11];
	input3 = readFloat(datain);//*((float*)(data3));
	arrayFloat[2] = input3;

	datain[0] = incomingBytes[12];
	datain[1] = incomingBytes[13];
	datain[2] = incomingBytes[14];
	datain[3] = incomingBytes[15];
	input4 = readFloat(datain);//*((float*)(data4));
	arrayFloat[3] = input4;

	datain[0] = incomingBytes[16];
	datain[1] = incomingBytes[17];
	datain[2] = incomingBytes[18];
	datain[3] = incomingBytes[19];
	input5 = readFloat(datain);//*((float*)(data5));
	arrayFloat[4] = input5;


	datain[0] = incomingBytes[20];
	datain[1] = incomingBytes[21];
	datain[2] = incomingBytes[22];
	datain[3] = incomingBytes[23];
	input6 = readFloat(datain);//*((float*)(data6));
	arrayFloat[5] = input6;


	datain[0] = incomingBytes[24];
	datain[1] = incomingBytes[25];
	datain[2] = incomingBytes[26];
	datain[3] = incomingBytes[27];
	input7 = readFloat(datain);//*((float*)(data7));
	arrayFloat[6] = input7;


	datain[0] = incomingBytes[28];
	datain[1] = incomingBytes[29];
	datain[2] = incomingBytes[30];
	datain[3] = incomingBytes[31];
	input8 = readFloat(datain);//*((float*)(data8));
	arrayFloat[7] = input8;

	datain[0] = incomingBytes[32];
	datain[1] = incomingBytes[33];
	datain[2] = incomingBytes[34];
	datain[3] = incomingBytes[35];
	input9 = readFloat(datain);//*((float*)(data9));
	arrayFloat[8] = input9;

	datain[0] = incomingBytes[36];
	datain[1] = incomingBytes[37];
	datain[2] = incomingBytes[38];
	datain[3] = incomingBytes[39];
	input10 = readFloat(datain);//*((float*)(data10));
	arrayFloat[9] = input10;
}

// funkcija za pretvaranje  4bytea  float 
float readFloat(byte datain[4])
{
	union
	{
		byte b[4];
		float f;
	} data;

	for (int i = 0; i < 4; i++)
	{
		data.b[i] = datain[i];
	}
	return data.f;
}

void sendSBUSandReadPPM() {
	//channels[i] = (uint16_t)(((float)ain[i]) * scaleFactor + bias);
	//	channels[i] = (uint16_t)(((float)ain[i]));
	// vrijednosti idu od 0 do 2000;
	channels[0] = radioChannels[0];
	channels[1] = radioChannels[1];
	channels[2] = radioChannels[2];
	channels[3] = radioChannels[3];
	channels[4] = radioChannels[4];
	channels[5] = radioChannels[5];
	channels[6] = radioChannels[6];
	channels[7] = radioChannels[7];
	channels[8] = 0;
	channels[9] = 0;
	channels[10] = 0;
	channels[11] = 0;
	channels[12] = 0;
	channels[13] = 0;
	channels[14] = 0;
	channels[15] = 0;
	SBUSToPX4.write(&channels[0]);
	readPPM(); // read ppm from receiver d4r-2
}

void readPPM() {
	radioChannels[0] = myIn.read(1);
	radioChannels[1] = myIn.read(2);
	radioChannels[2] = myIn.read(3);
	radioChannels[3] = myIn.read(4);
	radioChannels[4] = myIn.read(5);
	radioChannels[5] = myIn.read(6);
	radioChannels[6] = myIn.read(7);
	radioChannels[7] = myIn.read(8);

	//conditon readed values to  range of 0 to 200;
	//	value =map(value, fromLow, fromHigh, toLow, toHigh)
	radioChannels[0] = map(radioChannels[0], 991, 2013, 0, 2000);
	radioChannels[1] = map(radioChannels[1], 991, 2013, 0, 2000);
	radioChannels[2] = map(radioChannels[2], 991, 2013, 0, 2000);
	radioChannels[3] = map(radioChannels[3], 991, 2013, 0, 2000);
	radioChannels[4] = map(radioChannels[4], 991, 2013, 0, 2000);
	radioChannels[5] = map(radioChannels[5], 991, 2013, 0, 2000);
	radioChannels[6] = map(radioChannels[6], 991, 2013, 0, 2000);
	radioChannels[7] = map(radioChannels[7], 991, 2013, 0, 2000);
	
		}

int printSerialReadData() {

	Serial.print("Arm mode:    ");
	Serial.print(arrayFloat[0]);

	//Serial.print("  Data2  :");
	//Serial.print(arrayFloat[1]);

	Serial.print("   Thrust PWM from RC:    ");
	Serial.println(arrayFloat[3]);

	
	/*Serial.print("D1:");
	Serial.print(input1);
	Serial.print(" D2:");
	Serial.print(input2);
	Serial.print(" D3:");
	Serial.print(input3);
	Serial.print(" D4:");
	Serial.print(input4);
	Serial.print(" D5:");
	Serial.print(input5);
	Serial.print(" D6:");
	Serial.print(input6);
	Serial.print(" D7:");
	Serial.print(input7);
	Serial.print(" D8:");
	Serial.print(input8);
	Serial.print(" D9:");
	Serial.println(input9);*/
}

void printReceivedPPM() {
	Serial.print(channels[0]);
	Serial.print("  ");
	Serial.print(channels[1]);
	Serial.print("  ");
	Serial.print(channels[2]);
	Serial.print("  ");
	Serial.print(channels[3]);
	Serial.print("  ");
	Serial.print(channels[4]);
	Serial.print("  ");
	Serial.print(channels[5]);
	Serial.print("  ");
	Serial.print(channels[6]);
	Serial.print("  ");
	Serial.print(channels[7]);
	Serial.println("  ");

}

void printGlobalPPM() {
	Serial.print(radioChannels[0]);
	Serial.print("  ");
	Serial.print(radioChannels[1]);
	Serial.print("  ");
	Serial.print(radioChannels[2]);
	Serial.print("  ");
	Serial.print(radioChannels[3]);
	Serial.print("  ");
	Serial.print(radioChannels[4]);
	Serial.print("  ");
	Serial.print(radioChannels[5]);
	Serial.print("  ");
	Serial.print(radioChannels[6]);
	Serial.print("  ");
	Serial.print(radioChannels[7]);
	Serial.println("  ");

}
